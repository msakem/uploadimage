﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Text;
using OpenQA.Selenium.IE;
using System.Windows.Forms;
using System.Threading;

namespace UnitTestProject1
{

    [TestFixture]
    public class UntitledTestCase
    {
        private InternetExplorerDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;

        [SetUp]
        public void SetupTest()
        {
            
            //driver = new FirefoxDriver();
            //baseURL = "https://www.katalon.com/";
            verificationErrors = new StringBuilder();
            Proxy proxy = new Proxy { HttpProxy = "inetproxy:83", SslProxy = "inetproxy:83", Kind = ProxyKind.Manual, IsAutoDetect = false };

            var options = new InternetExplorerOptions
            {

                //UsePerProcessProxy = true,
                Proxy = proxy,
                 IntroduceInstabilityByIgnoringProtectedModeSettings = true,
                IgnoreZoomLevel = true,
                EnableNativeEvents = false
            };

           

            driver = new OpenQA.Selenium.IE.InternetExplorerDriver();
            driver.Manage().Window.Maximize();
            baseURL = "https://www.katalon.com/";
            verificationErrors = new StringBuilder();

        }

        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            NUnit.Framework.Assert.AreEqual("", verificationErrors.ToString());
        }

        [Test]
        public void TheUntitledTestCaseTest()
        {
            /*
             driver.Navigate().GoToUrl("http://nervgh.github.io/pages/angular-file-upload/examples/simple/");
            driver.FindElement(By.XPath("(//input[@type='file'])[2]")).SendKeys(@"C:\Program Files (x86)\Jenkins\workspace\upload1\UnitTestProject1\Data\aaaa.gif");

            SendKeys.SendWait(@"{Enter}");
            driver.FindElement(By.XPath("(//button[@type='button'])[4]")).Click();
            SendKeys.SendWait("");
            */
            /*
            driver.Navigate().GoToUrl("http://nervgh.github.io/pages/angular-file-upload/examples/simple/");
            driver.FindElement(By.CssSelector(".row .col-md-3 > input:nth-child(5")).SendKeys(@"C:\Program Files (x86)\Jenkins\workspace\upload1\UnitTestProject1\Data\aaaa.gif");
            SendKeys.SendWait(@"{Enter}");
            driver.FindElement(By.XPath("(//button[@type='button'])[4]")).Click();
            SendKeys.SendWait("");
            */
            driver.Navigate().GoToUrl("http://nervgh.github.io/pages/angular-file-upload/examples/simple/");
            IWebElement elem = driver.FindElement(By.CssSelector(".row .col-md-3 > input:nth-child(5"));
            String filePath1 = @"C:\Program Files (x86)\Jenkins\workspace\upload1\UnitTestProject1\Data\aaaa.gif";
            elem.SendKeys(filePath1);
             
        }
        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }
    }
}